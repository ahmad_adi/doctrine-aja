<?php
// list_bugs.php
require_once "bootstrap.php";

$dql = "SELECT a, b, x FROM Bug a JOIN a.engineer b JOIN a.reporter x ORDER BY a.created DESC";

$query = $entityManager->createQuery($dql);
$query->setMaxResults(30);
$bugs = $query->getResult();

foreach ($bugs as $bug) {
    echo $bug->getDescription()." - ".$bug->getCreated()->format('d.m.Y')."\n";
    echo "    Reported by: ".$bug->getReporter()->getName()."\n";
    echo "    Assigned to: ".$bug->getEngineer()->getName()."\n";
    foreach ($bug->getProducts() as $product) {
        echo "    - Product: ".$product->getName()."\n";
    }
    echo "\n";
}
