<?php

use Doctrine\Common\Collections\ArrayCollection;

// src/Product.php
/**
 * @Entity @Table(name="products")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 **/
class Product
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
    /** @Column(type="string") **/
    protected $name;
    
    /**
     * @Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;
    
    /**
     * @ManyToMany(targetEntity="Bug", mappedBy="products")
     **/
    protected $bugs;

    public function __construct()
    {
    	$this->bugs = new ArrayCollection();
    }
    
    public function __toString()
    {
    	return $this->name;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    //bugs :
    
    public function assignToBug($bug)
    {
    	$this->bugs[] = $bug;
    }
    
    public function getBugs()
    {
    	return $this->bugs;
    }
    
    /**
     * Bugs has many issues
     *
     * @OneToMany(targetEntity="Issue", mappedBy="product")
     *
     **/
    protected $issues;
    
    public function getDeletedAt()
    {
    	return $this->deletedAt;
    }
    
    public function setDeletedAt($deletedAt)
    {
    	$this->deletedAt = $deletedAt;
    }
    
}
