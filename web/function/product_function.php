<?php

function getProduct($product_id) {
	$entityManager = getEntityManager();
	$productRepository = $entityManager->getRepository('Product');
	$product = $productRepository->find($product_id);
	return $product;

}

function getAllProducts() {
	$entityManager = getEntityManager();
	$productRepository = $entityManager->getRepository('Product');
	$products = $productRepository->findAll();
	return $products;

}

//using many to many
function getAllProductsByBug($bug_id) {
	$entityManager = getEntityManager();
	
	$bugRepository = $entityManager->getRepository('Bug');
	$bug = $bugRepository->find($bug_id);
	
	$dql = "SELECT p, b FROM Product p JOIN p.bugs b";
	$dql = $dql . " WHERE b.id = ?1";
	
	$query = $entityManager->createQuery($dql)->setParameter(1, $bug->getId());
	
	$products = $query->getResult();
	return $products;

}

//using Issue
function getAllProductsByBug2($bug_id) {
	$entityManager = getEntityManager();

	$dql 	= "SELECT i, b, p FROM Issue i JOIN i.bug b JOIN i.product p";
	$dql 	= $dql . " where b.id = ?1";
	$query 	= $entityManager->createQuery($dql)->setParameter(1, $bug_id);
	
	$issues = $query->getResult();
	return $issues;

}

function deleteProduct($product_id) {
	$entityManager = getEntityManager();
	$productRepository = $entityManager->getRepository('Product');
	$product = $productRepository->find($product_id);
	//die(var_dump($product));
	$entityManager->getFilters()->enable('soft-deleteable');
	$entityManager->remove($product);
	$entityManager->flush();
}
