<?php 
	require "./layout/header.php";
	require './function/bug_function.php';
?>

<div class="panel panel-default">
  
  	<div class="panel-heading">Bugs</div>
	<div class="panel-body">

		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>Bug desc.</th>
					<th>created</th>
					<th>status</th>
					<th>engineer</th>
					<th>reporter</th>
					<th>Tot. products</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllBugsByProduct($_GET['product_id']) as $bug) {
		  		?>
		  			<tr>
		  				<td><?= $bug->getId() ?></td>
						<td><?= $bug->getDescription() ?></td>
						<td><?= $bug->getCreated()->format('d/m/Y'); ?></td>
						<td><?= $bug->getStatus() ?></td>
						<td><?= $bug->getEngineer() ?></td>
						<td><?= $bug->getReporter() ?></td>
						<td>
							<a href="product_by_bug.php?bug_id=<?= $bug->getId() ?>">
							products : 
							<?= count($bug->getProducts()) ?>
							</a>
						</td>
		  			</tr>
		  			<?php
		  				}
		  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>

<div class="panel panel-default">
  
  	<div class="panel-heading">Bugs</div>
	<div class="panel-body">

		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>Bug description</th>
					<th>Issue desc.</th>
					<th>created</th>
					<th>status</th>
					<th>engineer</th>
					<th>reporter</th>
					<th>Tot. products</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllBugsByProduct2($_GET['product_id']) as $issue) {
		  		?>
		  			<tr>
		  				<td><?= $issue->getBug()->getId() ?></td>
						<td><?= $issue->getBug()->getDescription() ?></td>
						<td><?= $issue->getDescription() ?></td>
						<td><?= $issue->getBug()->getCreated()->format('d/m/Y'); ?></td>
						<td><?= $issue->getBug()->getStatus() ?></td>
						<td><?= $issue->getBug()->getEngineer() ?></td>
						<td><?= $issue->getBug()->getReporter() ?></td>
						<td>
							<a href="product_by_bug.php?bug_id=<?= $issue->getBug()->getId() ?>">
							products : 
							<?= count($issue->getBug()->getProducts()) ?>
							</a>
						</td>
		  			</tr>
		  			<?php
		  				}
		  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>


<?php require "./layout/footer.php"?>
		
		
		
		
