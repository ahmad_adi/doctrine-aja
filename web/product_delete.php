<?php 
	require "./layout/header.php";
?>

<!-- Product -->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Product Profile</div>
	<div class="panel-body">
		<?php require './function/product_function.php'; ?>
		
		<?php 
			deleteProduct($_GET['product_id']); 
		?>
		product with id "<?php echo  $_GET['product_id'] ?>" deleted!
	  </div>

</div>

<?php require "./layout/footer.php"?>