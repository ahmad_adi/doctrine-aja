<?php 
	require "./layout/header.php";
?>

<!-- Product -->
<div class="panel panel-default">
  <!-- Default panel contents -->

  	<div class="panel-heading">Products</div>
	<div class="panel-body">
		  	<?php 
		  		require './function/product_function.php';
		  	?>
		    <table class="table table-condensed">
		  		<tr>
					<th>id</th>
					<th>Product Name</th>
					<th>total bugs</th>
		  		</tr>
		  				  		
		  		<?php 
		  			foreach (getAllProducts($_GET['bug_id']) as $product) {
		  		?>
		  			<tr>
		  				<td><?= $product->getId() ?></td>
						<td><?= $product->getName() ?></td>
						<td>
						<?php 
						//foreach ($product->getBugs() as $bug) {
					    	echo "    - Bug: ".count($product->getBugs())."\n";
					    //}
					    ?>
						</td>
		  			</tr>
	  			<?php
	  				}
	  			?>
		  		</tr>
		  		
			</table>
		
	  </div>

</div>

<?php require "./layout/footer.php"?>
		
		
		
		
