<?php
// create_bug.php
require_once "bootstrap.php";

$theReporterId = $argv[1];
$theDefaultEngineerId = $argv[2];
$productId = $argv[3];
$bugId = $argv[4];


$reporter = $entityManager->find("User", $theReporterId);
$engineer = $entityManager->find("User", $theDefaultEngineerId);


if (!$reporter || !$engineer) {
    echo "No reporter and/or engineer found for the input.\n";
    exit(1);
}

$product = $entityManager->find("Product", $productId);
if (!$product) {
	echo "product Not found.\n";
	exit(3);
}

$bug = $entityManager->find("Bug", $bugId);
if (!$bug) {
	echo "Bug Not found.\n";
	exit(2);
}



$bug->assignToProduct($product);

$bug->setReporter($reporter);
$bug->setEngineer($engineer);

$entityManager->persist($bug);
$entityManager->flush();

echo "Thank for reporting new bug(s), Your Bug Id: ".$bug->getId()."\n";
